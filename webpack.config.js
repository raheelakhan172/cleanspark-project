const { resolve } = require("path");
const webpack = require("webpack");

module.exports = {
    entry: "./Main.tsx",
    output: {
        path: resolve(__dirname, "dist"),
        filename: 'bundle.js'
    },
    context: resolve(__dirname, "components"),
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.json', '.css', '.scss', '.html', '.less']
    },
    module: {
        loaders: [
            {
                test: /\.tsx?/,
                use: [
                   {
                       loader: 'awesome-typescript-loader'
                   },
                ],

            }, 
            {
                test: /\.html$/, 
                loader: 'raw-loader'
            }
        ]
    }
}