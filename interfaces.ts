
export interface Tag {
  tagId: string;
  label: string;
  dataType: string;
  unit: string;
  isTransient: boolean;
  features: string[];
}

export interface TagData {
  observationTS: Date;
  tagId: string;
  value: number;
  quality: any; 
}

export interface Props<T> {
  data: T[];
  value: (item: T) => string;
  onClick?: (item: T | T[]) => void;
}