import * as React from 'react';
import { createBrowserHistory } from 'history'
import { ReactElement, ReactHTMLElement } from 'react';
import * as PropTypes from 'prop-types';
export class Router extends React.Component<{}, {}> {

    history =  createBrowserHistory();
    unsubscribe: any;

    static childContextTypes = {
        history: PropTypes.object
    };

    getChildContext() {
        return {
            history: this.history
        }
    }

    componentWillMount() {
        this.unsubscribe = this.history.listen(() => {
            this.forceUpdate();
        })
    }

    componentWillUnmount() {
        this.unsubscribe();
    }

    render() {
        return this.props.children;
    }
}

export class Route extends React.Component<RouteProps, {}> {
    static contextTypes = {
        history: PropTypes.object
    }

    render() {
        let {path, matchExact, render} = this.props;
        let {location} = this.context.history;
        
        let match = matchExact ? location.pathname === path : location.pathname.startsWith(path);

        if (match) {
            return render(this.context.history); 
        } else {
            return null;
        }
    }
}

class Link extends React.Component<Props, {}> {
    static contextTypes = {
        history: PropTypes.object
    }

    onClick = (e) => {
        e.preventDefault();
        this.context.history.push(this.props.to);
    }

    render() {
        return (
            <a 
                href={`${this.props.to}`}
                onClick={this.onClick}>
                {this.props.children}
            </a>
        )
    }
}


interface Props {
    to: string;
    beforeClick?: () => void;
}

interface RouteProps {
    path: string;
    matchExact: boolean;
    render?: (history) => JSX.Element;
}