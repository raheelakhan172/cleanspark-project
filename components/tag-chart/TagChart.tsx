import * as React from 'react';
import {TagData} from '../../interfaces';
import * as scale from 'd3-scale';
import * as time from 'd3-time';
import Visual from './TagChartVisual';
import * as d3 from 'd3';
import * as timeFormat from 'd3-time-format';

export default class TagChart extends React.Component<Props, {}> {

    static defaultProps = {
        timeFormat: (item: Date) => d3.timeFormat("%X")(item),
        dateFormat: (item: Date) => d3.timeFormat("%x")(item)
    }

    static margin = {
        top: 20,
        right: 20,
        bottom: 30,
        left: 80
    }

    static padding =  10;
    static width = 950 - TagChart.margin.left - TagChart.margin.right;
    static height = 500 - TagChart.margin.top - TagChart.margin.bottom;
    node: any;

    componentDidMount() {
        if (!this.props.loading) {
            this.append();
        }
    }

    componentDidUpdate() {
        this.append();
    }

    append() {
        d3.select(this.node)
            .datum(this.props.data)
            .call(Visual.enter, this.props.isBoolean);
    }

    xAxis() {
        let ticks = Visual.xAxis(5, true);
        if (!ticks.length) {
            ticks = Visual.xAxis(5, false);
        }
        return (
            <g>
                <g transform={`translate(0,${TagChart.height - (TagChart.margin.bottom)})`}>
                    {ticks.map((elem, ind) => {
                        let anchor = !ind ? "end" : "middle";
                        return (
                            <g 
                                key={ind}
                                className="tick"
                                transform={`translate(${Visual.x(elem)},25)`}>
                                    <text textAnchor={anchor}>
                                        {this.props.dateFormat(elem)}
                                    </text>
                                    <text y="20" textAnchor={anchor}>
                                        {this.props.timeFormat(elem)}
                                    </text>
                            </g>
                        )
                    })}
                </g>
                <line
                    x1={TagChart.margin.left}
                    x2={TagChart.width}
                    y1={TagChart.height - TagChart.margin.bottom + 6}
                    y2={TagChart.height - TagChart.margin.bottom + 6}
                    stroke="black"
                />
            </g>
        );
    }

    yAxis() {
        let ticks = Visual.yAxis(this.props.isBoolean ? 2 : 10); 
        return (
            <g transform={`translate(${TagChart.margin.left}, 6)`}>
                {ticks.map((elem, ind) => (
                    <g 
                        key={elem}
                        className="tick"
                        transform={`translate(0,${Visual.y(elem)})`}>
                            <g>
                                <text 
                                    textAnchor="end"
                                    transform={`translate(${-TagChart.padding},0)`}>
                                        {elem}
                                </text>
                            </g>
                    </g>
                ))}
                <line
                    x1={0}
                    x2={0}
                    y1={0}
                    y2={TagChart.height - TagChart.margin.bottom}
                    stroke="black"
                />
                <text
                    textAnchor="middle"
                    x={-50}
                    fontVariant="small-caps"
                    y={(TagChart.height - TagChart.margin.bottom * 2) / 2 - 5}
                    transform={`rotate(-90, -60, ${(TagChart.height - TagChart.margin.top - TagChart.margin.bottom) / 2})`}>

                    Unit in {this.props.unitValue}
                </text>
            </g>
        )
    }

    render() {
        if (!this.props.loading) {
            Visual.setX(new Date(this.props.minDate), new Date(this.props.maxDate), TagChart.margin.left, TagChart.width - TagChart.margin.right);
            Visual.setY(this.props.minValue < 0 ? this.props.minValue : 0, this.props.maxValue > 0 ? this.props.maxValue : 0, TagChart.margin.top, TagChart.height - TagChart.margin.bottom);

        }
        return (
            <div style={{height: TagChart.height + 50, position: 'relative'}}>
                {this.props.loading ? (
                    <div>Loading... </div>
                ) : (
                        <div>
                            <svg width={TagChart.width} height={TagChart.height + 50}>
                                {this.xAxis()}
                                {this.yAxis()}
                                <path
                                    ref={n => this.node = n}
                                    className="tagData" />
                            </svg>
                        </div>
                    )
                }
            </div>
        )
    }
}

interface Props {
    unitValue: string;
    loading: boolean;
    isBoolean: boolean;
    data: TagData[];
    minValue: number;
    maxValue: number;
    minDate: Date;
    maxDate: Date;
    timeFormat?: (item: Date) => string;
    dateFormat?: (item: Date) => string;
}

interface State {
    toolTip?: boolean;
}