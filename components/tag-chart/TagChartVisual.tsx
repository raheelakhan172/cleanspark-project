import * as d3 from "d3";
import * as scale from 'd3-scale';
import * as time from 'd3-time';
import {TagData} from '../../interfaces';

export default class TagChartVisual  {
    static x: any;
    static y: any;

    static enter(element: any, renderCurve?: boolean) {
        let line = d3.line()
            .x((d:TagData) => {
                return TagChartVisual.x(new Date(d.observationTS))
            })
            .y((d: TagData) => TagChartVisual.y(d.value))
            .curve(renderCurve ? d3.curveStepAfter : d3.curveLinear);

        element
            .attr("d", line)
            .attr("fill", "none")
            .attr("stroke", "#29434e")
            .attr("stroke-width", renderCurve ? 2 : 3);
    }

    static setX(minDate: Date, maxDate: Date, left: number, right: number) {
        TagChartVisual.x = scale.scaleTime()
            .domain([new Date(minDate), new Date(maxDate)])
            .range([left, right]);
    }

    static setY(minValue: number, maxValue: number, top: number, bottom: number) {
        TagChartVisual.y = scale.scaleLinear()
            .domain([minValue, maxValue]) 
            .range([bottom, top]);
    }

    static xAxis(step: number = 10, timeDate: boolean) {
        if (timeDate) {
            return TagChartVisual.x.ticks(d3.timeHour.every(step));
        }
        return TagChartVisual.x.ticks(step);
    }

    static yAxis(step: number = 10) {
        return TagChartVisual.y.ticks(step);
    }
}

interface TagChart {
    enter: (selection: any) => void;
    update?: (selection: HTMLElement) => void;
}