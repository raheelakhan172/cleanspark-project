import * as React from 'react';
import TagChartStore from './TagChartStore';
import {TagData} from '../../interfaces';
import TagChart from './TagChart';
import {Page} from '../Main';

export default class TagChartContainer extends React.Component<Props, State> {
    state: State = {
        loading: true,
        data: []
    };
    tagStore: TagChartStore;
    constructor(props: Props) {
        super(props);
        this.tagStore = new TagChartStore(props);
    }

    componentDidMount() {
        this.initialize(this.props.start, this.props.end);
    }

    initialize(start: Date, end: Date) {
        this.tagStore.initialize()
            .then(() => {
                this.setData(this.props.start, this.props.end);  
            });
    }

    setData(start: Date, end: Date) {
        this.setState({
            loading: this.tagStore.loading,
            data: this.tagStore.filter(start, end)
        })
    }

    componentWillReceiveProps(nextProps: Props) {
        if (nextProps.start !== this.props.start || nextProps.end !== this.props.end) {
            this.tagStore = new TagChartStore(nextProps);
            this.initialize(nextProps.start, nextProps.end);
        }
    }

    render() {
        return (
            <div className="container">
                <button onClick={() => this.props.history.push('/static/index.html')}> Back </button>
                <TagChart 
                    isBoolean={this.tagStore.disableLinearInterpolate}
                    loading={this.state.loading}
                    data={this.state.data}
                    minValue={this.state.loading ? null : this.tagStore.minValue()}
                    maxValue={this.state.loading ? null : this.tagStore.maxValue()}
                    minDate={this.state.loading ? null : this.tagStore.minDate() as any}
                    maxDate={this.state.loading ? null : this.tagStore.maxDate() as any}
                    unitValue={this.props.unitValue}
                />
            </div>
        )
    }
}

interface Props {
    unitValue: string;
    start: Date;
    end: Date;
    tagId: string;
    back: (which: Page) => void;
    timeFormat?: (item: Date) => string;
    dateFormat?: (item: Date) => string;
    history?: any;
}

interface State {
    loading: boolean;
    data: TagData[];
}