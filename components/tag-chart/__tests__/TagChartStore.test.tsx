import TagChartStore from '../TagChartStore';

let tagStore = new TagChartStore({tagId: 'tag1', start: new Date(), end: new Date()});
let tagData = [{"observationTS":"2017-11-29T23:59:00Z","tagId":"Tag1","value":0.99761250636122423,"quality":null},{"observationTS":"2017-11-29T23:58:02Z","tagId":"Tag1","value":0.99715890026061482,"quality":null}];


describe('Tag Chart', () => {
    it('should return empty array if there is no match', () => {
        let start = new Date();
        let end = new Date();

        let result = tagStore.filter(start, end);
        expect(result).toEqual([]);
    });

    it('should return correct range if match', () => {
        let startDate = new Date(Date.parse('2017-11-29T23:58:02Z'));
        let endDate = new Date(Date.parse('2017-11-29T23:59:00Z'));
        tagStore.initialize()
            .then(() => {
                let result = tagStore.filter(startDate, endDate);
                expect(result).toEqual(tagData);
            });
    })
})