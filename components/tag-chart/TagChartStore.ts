import get from '../../service/request';
//chart stuff, get data, and get it ready for viewing by chart
import {TagData} from '../../interfaces';
import TagChart from 'components/tag-chart/TagChart';

class TagChartStore {
    static valueMap = {
        'On': 1,
        'Off': 0,
        'true': 1,
        'false': 0
    };

    disableLinearInterpolate: boolean = false;
    
    loading: boolean = false;
    tagData: TagData[] = [];
    props: {};

    constructor(props: {tagId: string, start: Date, end?: Date}) {
        this.loading = true;
        this.props = props;
    }

    initialize() {
        return new Promise((resolve, reject) => {
        get('api/DataPoint', {...this.props})
            .then((d) => {
                this.normalizeData(d, resolve);
            })
            .catch((e) => console.warn(e));
        });
    }

    normalizeData(tag, resolve) {
        let temp = [...tag];
        let len = temp.length;
        
        for (let i = 0;  i < len; i++) {
            if (TagChartStore.valueMap[temp[i].value] !== undefined) {
                this.disableLinearInterpolate = true;
                temp[i].value = TagChartStore.valueMap[temp[i].value];
            } 
        }
        this.tagData = [...temp];
        this.loading = false;
        resolve();
    }

    filter(start: Date, end: Date) {
        return this.tagData.filter(it => new Date(it.observationTS) >= start && 
                                new Date(it.observationTS) <= end);
    }

    updateRange = (start: Date, end: Date) => {
        return this.tagData.filter(it => it.observationTS >= start && it.observationTS <= end);
    }

    minDate() {
        return Math.min(...this.tagData.map(it => new Date(it.observationTS)) as any);
    }

    maxDate() {
        return Math.max(...this.tagData.map(it => new Date(it.observationTS)) as any);
    }

    minValue() {
        return Math.min(...this.tagData.map(it => it.value));
    }

    maxValue() {
        return Math.max(...this.tagData.map(it => it.value));
    }
}

export default TagChartStore;