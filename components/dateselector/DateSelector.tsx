import * as React from 'react';

export default(props: Props) => {
    return (
        <input 
            value={props.value}
            type="datetime-local"
            min={props.minDate}
            max={props.maxDate}
            onChange={(e) => props.onSelect(e.target.value)}
        />
    )
}

interface Props {
    value: string;
    minDate: string;
    maxDate: string;
    onSelect: (date: string) => void;
}