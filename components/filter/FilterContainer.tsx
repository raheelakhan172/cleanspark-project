import * as React from 'react';
import * as ReactDom from 'react-dom';
import {Props} from '../../interfaces';
import TagPage from '../tagpage/TagPageContainer';
import {FilterStore} from './FilterStore';

function Filter<T>() {
  return class  extends React.Component<Props<T> & {filtersApplied?: T[]}, State<T>> {
    public static defaultProps = {
      filtersApplied: []
    }

    filterStore: FilterStore<T> = new FilterStore<T>();

    state: State<T> = {
      values: []
    }

    constructor(props: Props<T> & {filtersApplied?: T[]}) {
      super(props);
    }

    componentDidMount() {
      if (this.props.filtersApplied) {
        this.setState({
          values: this.props.filtersApplied
        })
      }
    }

    onClick = (filter: T) => {
      let val: T[] = this.filterStore.filter(filter, this.state.values);
      this.setState({
        values: val
      });
  
      this.props.onClick(val);
      
    }

    render() {
      return (
        <div 
          style={{backgroundColor: "#E1E2E1"}}
          className="container-row container-row-animated">
            <ul>
            {this.props.data.map((it,ind) => (
              <li
                onClick={() => this.onClick(it)}
                key={`${ind}_${it}`}>
                <input
                  type="checkbox"
                  checked={this.state.values.indexOf(it) === -1 ? false : true}
                  className='checkbox' />
                {this.props.value(it)}
              </li>

            ))}
          </ul>
        </div>
      )
    };
  }
} 


interface State<T> {
  values: T[];
}

export default Filter;
