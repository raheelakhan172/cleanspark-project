import {FilterStore} from '../FilterStore';

let filterStore = new FilterStore<string>();

describe('FilterStore', () => {
    it('should add filters correctly', () => {
        let filters = 'flower';
        let values = [];
        let result = filterStore.filter(filters, values);

        expect(result).toContain(filters);
    });

    it('should remove filter correctly', () => {
        let filter = 'flower';
        let values = [filter, 'power'];

        values = filterStore.filter(filter, values);
        expect(values).not.toContain(filter);
    });
});

