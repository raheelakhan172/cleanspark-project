
export class FilterStore<T> {

    filter(filter: T, values: T[]) : T[] {
        let index = values.findIndex(it => it === filter);
        let val = [];
        if (index === -1) {
          val = [...values, filter];
        } else {
          val = [...values];
          val.splice(index, 1);
        }
        return val;
    }
    
}