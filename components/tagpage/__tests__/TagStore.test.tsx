import {shallow} from "enzyme";
import * as TestUtils from "react-addons-test-utils";
import * as React from 'react';
import {TagPageStore} from "../TagPageStore";

let store = new TagPageStore();
let filters = ['flower', 'power'];
let tags = [
    {
        tagId: 'tag1',
        features: ['petals', 'yellow'],
        label: 'tag',
        unit: 'soil',
        isTransient: false,
        dataType: 'sun'
    },
    {
        tagId: 'tag2',
        features: ['flower'],
        label: 'tag',
        unit: 'soil',
        isTransient: false,
        dataType: 'sun'
    },
    {
        tagId: 'tag3',
        features: ['power'],
        label: 'tag',
        unit: 'soil',
        isTransient: false,
        dataType: 'sun'
    }
];


describe("TagPageStore", () => {
    it("selects correct data", () => {
        let store = new TagPageStore();
        let filters = ['flower', 'power'];

        let expectedTags = [
            {
                tagId: 'tag2',
                features: ['flower'],
                label: 'tag',
                unit: 'soil',
                isTransient: false,
                dataType: 'sun'
            },
            {
                tagId: 'tag3',
                features: ['power'],
                label: 'tag',
                unit: 'soil',
                isTransient: false,
                dataType: 'sun'
            }
        ];
        
        let result = store.selectData(filters, tags);

        expect(result).toEqual(expectedTags)
    });

    it("should extract filters correctly", () => {
        let val = ["petals", "yellow", "flower", "power"];

        let result = store.setFilters(tags);
        expect(result).toEqual(val);
    })
    
});
