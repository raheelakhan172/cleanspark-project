import * as React from 'react';
import * as ReactDom from 'react-dom';
import {Tag} from '../../interfaces';
import List from '../list/List';
import Filter from '../filter/FilterContainer';
import DateSelector from '../dateselector/DateSelector';
import {TagPageStore} from './TagPageStore';

export default class extends React.Component<Props,State> {

    state: State = {
        loading: false,
        filterSelected: false,
        appliedFilters: []
    }

    tagStore: TagPageStore = new TagPageStore();

    filters: string[] = [];

    tags: Tag[] = [];

    constructor(props: Props) {
        super(props);
    }

    componentDidMount() {
        this.tagStore.load()
            .then((d: Tag[]) => {
                this.tags = d;
                this.setFilters(d);
            })
            .catch((e) => console.warn(e))
            .then(() => this.setState({
                loading: false
            }));
    }

    applyFilters = (filters: string[]) => {
        this.setState({
            appliedFilters: filters
        });
    }

    selectData() {
        return this.tagStore.selectData(this.state.appliedFilters, this.tags);
    }
    
    setFilters(tags: Tag[]) { 
        this.filters = this.tagStore.setFilters(tags);
    }

    render() {
        let Filters = Filter<string>();
        return (
            this.state.loading ? <div>Loading ... </div>
                : (
                    <div className="container">
                        <button onClick={() => this.setState({filterSelected: !this.state.filterSelected})}> View Filters</button>
                        {this.state.filterSelected && (
                            <Filters
                                value={(item) => item}
                                data={this.filters}
                                onClick={this.applyFilters}
                                filtersApplied={this.state.appliedFilters}
                            />
                        )}
                        {List<Tag>({
                            value: (item) => item.label,
                            data: this.selectData(),
                            onClick: (item: Tag) => {
                                this.props.history.push('/details');
                                this.props.select(item.tagId, item.unit);
                            },
                            unit: (item: Tag) => item.unit
                        })}
                    </div>
                ))
    }            
}

interface Props {
    selectedItem?: () => void;
    select: (tagId: string, unit: string) => void;
    history?: any;
}

interface State {
    appliedFilters: string[];
    loading: boolean;
    filterSelected: boolean;
}

