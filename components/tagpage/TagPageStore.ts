import get from '../../service/request';
import {Tag} from '../../interfaces';

export class TagPageStore {
    filters: string[] = [];

    load() {
        return new Promise((resolve, reject) => {
            get('api/Tag', {})
            .then(d => {
                resolve(d);
            })
            .catch(e => reject(e));
        });
    }

    setFilters(tags: Tag[]) : string[] {
        for (let i = 0; i < tags.length; i++) {
            let { features } = tags[i];
            for (let j = 0; j < features.length; j++) {
                if (this.filters.indexOf(features[j]) === -1) {
                    this.filters.push(features[j]);
                }
            }
        }
        return this.filters;
    }

    selectData(appliedFilters: string[], tagData: Tag[]) : Tag[] {
        let tags = {};
        let data:Tag[] = [];
        if (appliedFilters.length) {
            for (let i = 0; i < tagData.length; i++) {
                for (let j = 0; j < appliedFilters.length; j++) {
                    if (tagData[i].features.indexOf(appliedFilters[j]) !== -1 && !tags[tagData[i].tagId]) {
                        tags[tagData[i].tagId] = true;
                        data.push(tagData[i]);
                    }
                }
            }
        } else {
            data = tagData;
        }
        return data;
    }
}