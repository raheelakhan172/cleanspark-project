import * as React from 'react';
import * as ReactDom from 'react-dom';
import TagPage from './tagpage/TagPageContainer';
import TagChart from './tag-chart/TagChartContainer';
import DateSelector from './dateselector/DateSelector';
import {Router, Route} from './router/Router';

class Main extends React.Component<{}, State> {  //change to router
    state: State = {
        currentView: Page.One,
        maxDate: new Date('2017-11-30T12:00'),
        minDate: new Date('2017-11-28T12:00'),
    }
    
    minDate = '2017-10-15T12:00';
    maxDate = '2017-11-30T12:00';

    selectedData: {};
    
    updatePage = (which: Page) => {
        this.setState({
            currentView: which
        })
    }

    formatDate(date: Date) {
        let day = date.getDate() < 10 ? `0${date.getDate()}` : `${date.getDate()}`;
        let hours = date.getHours() < 10 ? `0${date.getHours()}` : `${date.getHours()}`;
        let minutes = date.getMinutes() < 10 ? `0${date.getMinutes()}` : `${date.getMinutes()}`;
        return `${date.getFullYear()}-${date.getMonth() + 1}-${day}T${hours}:${minutes}`;
    }

    setMinDate = (date: string) => {
        if (date) {
            this.setState({
                minDate: new Date(date)
            });
        }
    }

    setMaxDate = (date: string) => { 
        if (date) {
            this.setState({
                maxDate : new Date(date)
            })
        }
    }


    setData = (tagId: string, unit: string) => {
        this.selectedData = {
            tagId: tagId,
            unitValue: unit,
            start: this.state.minDate,
            end: this.state.maxDate
        };
        this.updatePage(Page.Two);
    }

    render() {
        return (
        <div className="container">
            <div 
                style={{
                        borderBottom: '1px solid #29434e',
                        marginBottom: '0.5em'
                    }}
                className="container-row justify-start">
                <DateSelector
                    value={this.formatDate(this.state.minDate)}
                    minDate={this.minDate}
                    maxDate={this.formatDate(this.state.maxDate)}
                    onSelect={this.setMinDate}
                />
                <DateSelector
                    value={this.formatDate(this.state.maxDate)}
                    minDate={this.formatDate(this.state.minDate)}
                    maxDate={this.maxDate}
                    onSelect={this.setMaxDate}
                />
            </div>
            <Router>
                <Route 
                    path="/static/index.html" 
                    render={(history) => <TagPage select={this.setData} history={history}/>} 
                    matchExact={true}
                />
                <Route
                    path="/details"
                    render={(history) => 
                    <TagChart 
                        {...this.selectedData as any}  
                        start={this.state.minDate}
                        end={this.state.maxDate}
                        history={history}
                    />}
                    matchExact={true}
                />
           </Router> 
        </div>
        );
    }
}
interface State {
    currentView: Page;
    maxDate: Date;
    minDate: Date;
}

export enum Page {
    One = 1,
    Two = 2
};

ReactDom.render(
    <Main/>,
    document.getElementById("main")
) 

