import * as React from 'react';
import {Props} from '../../interfaces';

function List<T>(props: ListProps<T>) {
    let {data} = props;
    return (
        <div id="tags">  
        {props.data.map((item,ind) => (
            <div 
                key={ind}
                className="item" 
                onClick={() => props.onClick(item)}>
                <div className="item__header">
                    {props.value(item)}
               </div>
               <div className="item__value">
                    <span> Unit: </span> <span className="upper">{props.unit(item)} </span>
                </div>
            </div>
        ))}
        </div>
    )
}

export default List;

interface ListProps<T> extends Props<T> {
    unit: (item: T) => string;
}
