# CleanSpark Project
  Let's see those sparks! ^_^

## FOR WINDOWS USERS:
You will need to install Python 2.7.4 due to a Jest module relying on Python.
Head over to https://www.python.org/download/releases/2.7.4/ and install the version for your machine.
Be sure to set the path if you installed Python differently since `node-gyp` will look for Python in the default path.

Afterwards, if you ran `npm start` in the `clean-spark` directory before installing Python, remove `node_modules` from the directory and re-run `npm install`. 

Step 1:
`cd` into project directory and run `npm install`

Step 2:
To start tests, type `npm test`

Step 3:
To run project, type `npm start`
After webpack finishes compiling, navigate to `localhost:8080/static/index.html` 

