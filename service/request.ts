'use strict';


const get = (sub: string, options: any) => {
    let main = `http://cs-mock-timeseries-api.azurewebsites.net/${sub}`;
    if (Object.keys(options).length) {
        main += `/${options.tagId}?startTS=${new Date(options.start).toISOString()}`;
        if ('end' in options) {
            main += `&endTS=${new Date(options.end).toISOString()}`;
        }
    }
    
    return new Promise((resolve, reject) => {
        let request = new XMLHttpRequest();
        request.open("GET", main);
        request.onreadystatechange = () => {
           if (request.readyState === XMLHttpRequest.DONE && request.status === 200) {
               resolve(JSON.parse(request.response));
           }
        }

        request.onerror = () => {
            reject(request.statusText);
        }
        
        request.send();

    });
}

export default get;